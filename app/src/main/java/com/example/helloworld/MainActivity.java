package com.example.helloworld;

import android.os.AsyncTask;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.lang.ref.WeakReference;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void pressMeClick(View view) {
        String s = ((EditText)findViewById(R.id.input)).getText().toString();
        ((TextView)findViewById(R.id.textview)).setText("Hello" + s);
    }

    public void startAsync(View v) {
        MyAsync m = new MyAsync(this);
        m.execute();
    }

    class MyAsync extends AsyncTask<Void,Void,Void> {

        WeakReference<MainActivity> parent;

        public MyAsync(MainActivity parent) {
            this.parent = new WeakReference(parent);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            for (int i = 0; i < 1000000; i++) {

                for (int j = 0; j < 1000; j++) {

                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            parent.get().showTost();
        }
    }

    private void showTost() {
        Toast.makeText(this, R.string.done, Toast.LENGTH_LONG).show();
        findViewById(R.id.progress).setVisibility(View.GONE);
    }

}